angular
  .module("Services", [
  ])
  .factory("broadcast", [
    "$rootScope",
    "$timeout",
    function ($rootScope, $timeout) {
      return function (event, message) {
        $rootScope.$apply(function () {
          $rootScope.$broadcast(event, message);
        });
      };
    }
  ])
  .factory("server", [
    "$rootScope",
    "$cookies",
    "$timeout",
    "broadcast",
    function ($rootScope, $cookies, $timeout, broadcast) {
      var ws = null
        , server = {};

      server.init = function () {
        ws = new WebSocket("wss://sonnenkarma.de:24025");

        ws.onopen = function () {
          var flash =
            "Hallo! Ich bin Eddie, dein karmischer Begleiter. " +
            "Ich habe uns soeben mit dem Kosmos verbunden und " +
            "werde dich an dieser Stelle stets über die Folgen " +
            "deiner Aktionen aufklären. Viel Spaß! ;)";
          broadcast("flash", flash);
        };

        ws.onerror = function (err) {
          var error =
            "Leider muss ich dir mitteilen, dass es einen Fehler " +
            "in unserer karmischen Verbindung gab! ;( " +
            "Versuche, doch mal die Seite neu zu laden.";
          broadcast("error", error);
          broadcast("cleanup");
          console.debug(err);
        };

        ws.onclose = function (code, message) {
          var error =
            "Hmm... Irgendwie wurde unsere Verbindung zum Kosmos " +
            "getrennt! Hast du vielleicht versucht, unrechtmäßig " +
            "dein Karma zu beeinflussen? :D";
          broadcast("error", error);
          broadcast("cleanup");
          console.debug(code, message);
        };

        ws.onmessage = function (message) {
          try {
            var response = JSON.parse(message.data);
            console.debug(response);

            var channel = null,
              username = null,
              count = null;

            switch (response.shift()) {
              case  0:
                broadcast("error", response.shift());
                break;
              case  1:
                broadcast("flash", response.shift());
                break;
              case 12:
                broadcast("authenticate", response.shift());
                break;
              case 15:
                username = response.shift();
                channel = response.shift();
                count = response.shift();

                var flash =
                  "Hurra! Wir haben uns erfolgreich in das Karma von " +
                  "'" + channel + "' eingeklinkt!";
                /*
                if (count > 1) {
                  flash = flash + " " +
                    "Außer uns, wissen das noch " + count + " andere...";
                }
                */
                broadcast("flash", flash);
                break;
              case 17:
                channel = response.shift();
                count = response.shift();

                console.debug(channel, count);
                break;
              case 18:
                channel = response.shift();
                var when = response.shift(),
                  content = response.shift();

                  broadcast("message", [channel, when, content]);
                console.debug(channel, when, content);
                break;
              case 22:
                var channels = JSON.parse(response.shift());
                broadcast("channels", channels);
                console.debug(channels);
                break;
              case 24:
                var images = JSON.parse(response.shift());
                broadcast("images", images);
                console.debug("imagelist count", images.length);
                break;
              case 26:
                var image = response.shift();
                broadcast("images", image);
                console.debug("added image", image);
                break;
              default:
                var error =
                  "Du, ich muss dir sagen, dass wir eine ungewöhnliche " +
                  "kosmische Schwingung empfangen haben! Leider ist das " +
                  "garnicht so gut... ;)";
                throw new Error(error);
            }
          } catch (ex) {
            broadcast("error", ex.message);
          }
        };

        WebSocket.prototype.count = 0;

        WebSocket.prototype.request = function () {
          var args = [].slice.call(arguments);

          var self = this;

          var request = JSON.stringify(args);
          if (self.readyState === 1) {
            self.send(request);
            console.debug(request);
          } else {
            var flash =
              "So was aber auch... Der Kosmos hat unsere Aktion irgendwie " +
              "nicht registriert! Egal - ich probiere es gleich nochmal...";
            broadcast("flash", flash);
            $timeout(function () {
              self.count++;
              if (self.count < 5) {
                self.request(args);
              } else {
                var error =
                  "Scheinbar ist der Kosmos verschwunden... ;(";
                broadcast("error", error);
              }
            }, 1000);
          }
        };
      };

      server.register = function (username, password) {
        ws.request(10, username, password);
      };

      server.authenticate = function (username, password) {
        ws.request(11, username, password);
        $timeout(function () {
          server.authenticate(username, password);
        }, 10*60*1000);
      };

      server.signout = function (sessionId) {
        ws.request(13, sessionId);
      };

      server.subscribe = function (sessionId, channel) {
        ws.request(14, sessionId, channel);
      };

      server.publish = function (sessionId, channel, content) {
        ws.request(16, sessionId, channel, content);
      };

      server.unsubscribe = function (sessionId, channel) {
        ws.request(19, sessionId, channel);
      };

      server.unregister = function (sessionId) {
        ws.request(20, sessionId);
      };

      server.channels = function (sessionId) {
        ws.request(21, sessionId);
      };

      server.images = function (sessionId) {
        ws.request(23, sessionId);
      };

      server.addImage = function (sessionId, url) {
        ws.request(25, sessionId, url);
      };

      return server;
    }
  ]);
