angular.module("Relais-3")
  .directive("r3Messages", function () {
    return {
      restrict: "E",
      scope: {
        channel: "="
      },
      templateUrl: "templates/r3-messages.html"
    };
  })
  .directive("r3Editor", function () {
    return {
      restrict: "E",
      scope: {
        channel: "=",
        preview: "=",
        smilepad: "=",
        md: "="
      },
      templateUrl: "templates/r3-editor.html",
      link: function (scope, elem, attrs) {
        scope.togglePreview = function () {
          console.log("togglePreview");
          if (scope.channel.subscribed) {
            scope.smilepad.show = false;
            scope.md.show = false;
            scope.preview.show = !scope.preview.show;
          }
        };

        scope.toggleSmilepad = function () {
          console.log("toggleSmilepad");
          if (scope.channel.subscribed) {
            scope.preview.show = false;
            scope.md.show = false;
            scope.smilepad.show = !scope.smilepad.show;
          }
        };

        scope.toggleMdHelp = function () {
          console.log("toggleMdHelp");
          if (scope.channel.subscribed) {
            scope.preview.show = false;
            scope.smilepad.show = false;
            scope.md.show = !scope.md.show;
          }
        };

        scope.disabled = function () {
          return !scope.channel.subscribed;
        };

        scope.$on("insert", function (event, idx) {
          if (scope.channel.selected) {
            if (!scope.content) scope.content = "";
            scope.content += scope[attrs.smilepad].content[idx];
          }
        });
      },
      controller: function ($rootScope, $scope, server) {
        $scope.publish = function (channel, content) {
          if (content && content.length) {
            server.publish($rootScope.sessionId, channel, JSON.stringify({
              sender: $rootScope.username,
              content: content
            }));
            $scope.content = "";
            $scope.preview.show = false;
            $scope.smilepad.show = false;
            $scope.md.show = false;
          }
        };

        $scope.addImage = function ($event, url) {
          if ($event.which === 13 && url) {
            server.addImage($rootScope.sessionId, url);
            $event.target.value = "";
          }
        };
      }
    }
  })
  .directive("r3Enter", function ($parse, $timeout) {
    return {
      restrict: "A",
      link: function (scope, elem, attrs) {
        var converter = new Markdown.Converter();

        scope.$watch(attrs.ngModel, function (newVal, oldVal) {
          if (newVal !== oldVal) {
            scope[attrs.preview].content = converter.makeHtml(newVal);
          }
        }, true);

        elem.bind("keypress", function (ev) {
          if (ev.which === 10 && ev.ctrlKey) {
            scope.$apply(function () {
              scope[attrs.preview].show = !scope[attrs.preview].show;
            });
            ev.preventDefault();
          } else if (ev.which === 13 && ev.shiftKey) {
            scope.$apply(function () {
              scope[attrs.ngModel] += "\n";
            });
            ev.preventDefault();
          } else if (ev.which === 13) {
            scope.$apply(function () {
              scope.$eval(attrs.r3Enter);
            });
            ev.preventDefault();
          }
        });

        elem.bind("blur", function (ev) {
          scope.$apply(function () {
            scope[attrs.preview].show = false;
          });
          ev.preventDefault();
        });
      }
    };
  })
  .directive("r3Tooltip", function ($timeout) {
    return {
      restrict: "A",
      link: function (scope, elem, attrs) {
        scope.$on("help", function (ev, newVal) {
          scope.help = newVal;
        });

        elem.bind("mouseenter", function (ev) {
          if (scope.help) {
            elem.addClass("help-border");
            alertify.log(attrs.r3Tooltip, "", 0);
          }
          ev.preventDefault();
        });

        elem.bind("mouseleave", function (ev) {
          if (scope.help) {
            elem.removeClass("help-border");
            angular.element(".alertify-log.alertify-log-show").remove();
          }
          ev.preventDefault();
        });
      }
    };
  });
