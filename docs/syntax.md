## Markdown-Syntax

Eine ausführliche Beschreibung findest du hier:
<a href="http://markdown.de/syntax/" target="_blank">markdown.de</a>

### 1. Absätze

- Absätze werden durch einfache Leerzeilen getrennt.

*Bsp:*

    Absatz 1

    Absatz 2

*ergibt:*

Absatz 1

Absatz 2

### 2. Schriftgröße

- es gibt 6 Schriftgrößen (= Überschriften), welche durch das
  'Raute'-Symbol gekennzeichnet werden. Dabei gilt: Je mehr Rauten vor
  den Text gestellt werden, desto kleiner wird die Schrift.

*Bsp:*

    # größte Überschriftengröße

    ### mittlere Überschriftengröße

    normale Schriftgröße

*ergibt:*

# größte Überschriftengröße

### mittlere Überschriftengröße

normale Schriftgröße

### 3. Fett- und Kursivgedruckte Schriftart

*Bsp:*

    **Fett**

    *Kursiv*

*ergibt:*

**Fett**

*Kursiv*

### 4. Links

*Bsp:*

    <http://sonnenkarma.de/relais>

    [relais](http://sonnenkarma.de/relais)

*ergibt:*

<http://sonnenkarma.de/relais>

[relais](http://sonnenkarma.de/relais)

**WICHTIG:**
Der Link muss mit dem zu benutzenden Protokoll beginnen. Also z.B.
*http://* oder *ftp://*.

### 5. Bild-Links

*Bsp:*

    ![alternativer Text](http://sonnenkarma.de/projects/relais/pictures/background.png)

*ergibt:*

![alternativer Text](http://sonnenkarma.de/projects/relais/pictures/background.png)

**WICHTIG:**
Du *musst* für dein Bild einen "alternativen Text" angeben!

### 6. Unformatierter Text

- Wenn Text als unformatiert bzw. als Code dargestellt werden soll,
  musst du ihn um 4 Leerzeichen einrücken und in einen eigenen Absatz
  packen.
- Um einzelne Wörter unformatiert darzustellen, musst du ihn in
  "Backticks" (\`) setzen.

*Bsp:*

    Ich bin ein unformatierter Absatz. Ich kann z.B. Links im "Roh"-Format
    oder das Raute-Symbol ohne Auswertung durch Markdown enthalten:

    [relais](http://sonnenkarma.de/relais)

    ### 3 mal Raute ist jetzt nicht "mittlere Überschrift"

    `#` 1 mal Raute

*ergibt:*

    Ich bin ein unformatierter Absatz. Ich kann z.B. Links im "Roh"-Format
    oder das Raute-Symbol ohne Auswertung durch Markdown enthalten:

    [relais](http://sonnenkarma.de/relais)

    ### 3 mal Raute ist jetzt nicht "mittlere Überschrift"

`#` 1 mal Raute

**HINWEIS:**
Um z.B. ein Wort in Sternchen zu setzen, musst du vor die Sternchen ein
Escape-Zeichen (\\) schreiben:

    \*Wort\*

### 7. Zitate

*Bsp:*

    > Ich bin ein Zitat.
    > > Ich wurde in einem Zitat zitiert.

*ergibt:*

> Ich bin ein Zitat.
> > Ich wurde in einem Zitat zitiert.

### 8. Listen

*Bsp:*

    - Rot
    - Grün
    - Blau

    * Apfel
    * Orange
    * Zitrone

    1. Punkt 1
    2. Punkt 2
    3. Punkt 3

*ergibt:*

- Rot
- Grün
- Blau

* Apfel
* Orange
* Zitrone

1. Punkt 1
2. Punkt 2
3. Punkt 3

### 9. Horizontale Linien

- Horizontale Linien können durch 3 oder mehr Bindestriche erzeugt
  werden.

*Bsp:*

    Absatz 1

    ---

    Absatz 2

    --------

    Absatz 3

*ergibt:*

Absatz 1

---

Absatz 2

--------

Absatz 3
