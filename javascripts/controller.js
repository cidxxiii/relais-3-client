angular.module("Relais-3")
  .controller("RelaisCtrl", function ($rootScope, $scope, $timeout, $cookies, $location, server) {
    $scope.$on("error", function (event, error) {
      alertify.error(error);
    });

    $scope.$on("flash", function (event, flash) {
      alertify.success(flash);
    });

    $scope.$on("authenticate", function (event, sessionId) {
      if (!$rootScope.sessionId) {
        $rootScope.sessionId = sessionId;
        $rootScope.$broadcast("flash", "Herzlich Willkommen! :)");
        $cookies.sessionId = $rootScope.sessionId;
        $location.path("/");
        $rootScope.$broadcast("audio", "signin");
      }
      $timeout(function () {
        server.images(sessionId);
      }, 500);
    });

    $scope.$on("cleanup", function (event) {
      delete $rootScope.sessionId;
      delete $rootScope.username;
      if ($cookies.sessionId) {
        delete $cookies.sessionId;
      }
      if ($cookies.username) {
        delete $cookies.username;
      }
    });

    $scope.$on("audio", function (event, type) {
      if (!$scope.muted && $scope.audio.hasOwnProperty(type)) {
        $scope.audio[type].play();
      }
    });

    $scope.init = function () {
      moment.lang("de");
      if ($cookies.sessionId) {
        $rootScope.sessionId = $cookies.sessionId;
      }
      if ($cookies.username) {
        $rootScope.username = $cookies.username;
      }
      $scope.muted = false;
      if ($cookies.muted) {
        $scope.muted = $cookies.muted === "true" ? true : false;
      }
      server.init();
      if ($rootScope.sessionId && $rootScope.username) {
        $timeout(function () {
          server.authenticate($rootScope.username, $rootScope.sessionId);
        }, 1000);
      }
      $scope.help = false;
      $rootScope.filter = null;
      $scope.audio = {
        signin:  new Audio("sounds/SkypeLogin.wav"),
        signout: new Audio("sounds/SkypeLogout.wav"),
        message: new Audio("sounds/ChatIncoming.wav"),
        unread:  new Audio("sounds/ChatIncomingInitial.wav"),
        online:  new Audio("sounds/ContactOnline.wav"),
        offline: new Audio("sounds/ContactOffline.wav")
      }
      console.debug("RelaisCtrl initialized");
    };

    $scope.doSignout = function () {
      server.signout($rootScope.sessionId);
      $rootScope.$broadcast("cleanup");
      $rootScope.$broadcast("audio", "signout");
    };

    $scope.toggleHelp = function () {
      $scope.help = !$scope.help;
      $rootScope.$broadcast("help", $scope.help);
      if ($scope.help) {
        alertify.log("TIPP: Fahre mit der Maus über die einzelnen Seitenelemente während die Hilfe aktiviert ist!");
      } else {
        angular.element("#help").removeClass("help-border");
        angular.element(".alertify-log.alertify-log-show").remove();
      }
    };

    $scope.toggleAudio = function () {
      $scope.muted = !$scope.muted;
      $cookies.muted = $scope.muted.toString();
    };
  })
  .controller("RegisterCtrl", function ($rootScope, $scope, $cookies, $location, server) {
    $scope.doCancel = function () {
      $location.path("/");
    };

    $scope.doRegister = function (user) {
      if (!user.username || !user.password) {
        return $rootScope.$broadcast("error", "Bitte gib deinen Benutzernamen UND dein Passwort ein!");
      }

      if (!user.control) {
        return $rootScope.$broadcast("flash", "Bitte gib dein Passwort zur Kontrolle erneut ein!");
      }

      if (user.password !== user.control) {
        return $rootScope.$broadcast("error", "Oje! Die eingegebenen Passwörter stimmen leider nicht überein!");
      }

      server.register(user.username, user.password);
      $rootScope.username = user.username;
      $cookies.username = $rootScope.username;
    };
  })
  .controller("SigninCtrl", function ($rootScope, $scope, $cookies, $location, server) {
    $scope.doAuthenticate = function (user) {
      if (!user.username || !user.password) {
        return $rootScope.$broadcast("error", "Bitte gib deinen Benutzernamen UND dein Passwort ein!");
      }

      server.authenticate(user.username, user.password);
      $rootScope.username = user.username;
      $cookies.username = $rootScope.username;
    };

    $scope.doCancel = function () {
      $location.path("/");
    };
  })
  .controller("MainCtrl", function ($rootScope, $scope, $timeout, $cookies, $http, $filter, server) {
    $scope.init = function () {
      $scope.channels = [];
      $scope.preview = {
        show: false,
        content: ""
      };
      $scope.smilepad = {
        show: false,
        content: []
      };
      $scope.md = {
        show: false,
        content: null
      };
      $http.get("docs/syntax.md")
        .success(function (data, status) {
          $scope.md.content = data;
        })
        .error(function (data, status) {
          console.log("error", data, status);
        });
      $rootScope.counter = 0;
      $scope.$watch(function () { return $rootScope.counter; }, function (newVal, oldVal) {
        if (!_.isNull(newVal)) {
          var s = "relais-&lt;3@sonnenkarma.de"
            , title = newVal ? "(" + newVal + ") " + s : s;
          angular.element("title").html(title);
        }
      });

      $scope.$watch(function () { return $scope.channels.length; }, function (newVal, oldVal) {
        if (newVal !== oldVal) {
          var type = newVal > oldVal ? "online" : "offline";
          $rootScope.$broadcast("audio", type);
        }
      });
    };

    $scope.$on("channels", function (event, channels) {
      var current = angular.element("#main-menu")[0].selected;
      current = current > -1 ? current : 0;

      if (!$scope.channels) $scope.init();
      _.each(channels, function (channel) {
        if (!_.findWhere($scope.channels, { name: channel })) {
          $scope.channels.push({
            name: channel,
            messages: [],
            subscribed: false,
            selected: false,
            unread: 0
          });
        }
      });
      _.each($scope.channels, function (channel) {
        if (!_.contains(channels, channel.name)) {
          if (channel.selected) {
            $scope.select(0);
          }
          $scope.channels.splice(_.indexOf($scope.channels, channel), 1);
        }
      });
      if ($scope.channels.length) {
        if ($cookies.channel) {
          _.each($scope.channels, function (channel, idx) {
            if (channel.name === $cookies.channel) {
              $scope.select(idx);
            }
          });
        } else if (current) {
          $scope.select(current);
        }
      }
    });

    $scope.select = function (idx) {
      angular.element("#main-menu")[0].selected = idx;
      angular.element("#main-pages")[0].selected = idx;
      //$scope.preview.show = false;
      $cookies.channel = $scope.channels[idx].name;
      _.each($scope.channels, function (channel) {
        channel.selected = false;
      });
      $scope.channels[idx].selected = true;
      $scope.channels[idx].unread = 0;
      $scope.setCounter();
    };

    $scope.toggle = function ($event, channel) {
      if (!$event.target.value) {
        server.subscribe($rootScope.sessionId, channel);
      } else {
        server.unsubscribe($rootScope.sessionId, channel);
      }
      $scope.channels.forEach(function (ch) {
        if (ch.name === channel) {
          ch.subscribed = !$event.target.value;
          $scope.preview.show = false;
          $scope.smilepad.show = false;
          $scope.md.show = false;
        }
      });
    };

    $scope.$on("message", function (event, message) {
      $timeout(function () {
        var obj = {
          when: moment(message[1]).format("dddd, Do MMM YYYY, HH:mm:ss"),
          message: JSON.parse(message[2])
        };
        _.each($scope.channels, function (channel) {
          if (channel.name === message[0] && channel.subscribed) {
            channel.messages.push(obj);
            $scope.preview.show = false;
            $scope.smilepad.show = false;
            $scope.md.show = false;
            $timeout(function () {
              var id = "#" + channel.name + (channel.messages.length - 1);
              console.log(obj, id);
              if (channel.selected) {
                angular.element(id)[0].scrollIntoView();
                $rootScope.$broadcast("audio", "message");
              } else {
                channel.unread++;
                $scope.setCounter();
                $rootScope.$broadcast("audio", "unread");
              }
            }, 100);
          }
        });
      }, 500);
    });

    $scope.messages = function (channel) {
      _.each($scope.channels, function (ch) {
        if (ch.name === channel && ch.messages.length) {
          console.log(ch.messages);
          return ch.messages;
        }
      });
    };

    $scope.insert = function (idx) {
      $rootScope.$broadcast("insert", idx);
    };

    $scope.$on("images", function (event, images) {
      console.log(images);
      if (_.isArray(images)) {
        $scope.smilepad.content = images;
      } else if (_.isString(images) && !_.contains($scope.smilepad.content, images)) {
        $scope.smilepad.content.push(images);
      }
    });

    $scope.setCounter = function () {
      $rootScope.counter = 0;
      _.each($scope.channels, function (channel) {
        $rootScope.counter += channel.unread;
      });
    };
  });
