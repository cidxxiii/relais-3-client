angular.module("Filters", [])
  .filter("markdown", function () {
    return function (input) {
      var converter = new Markdown.Converter();
      return converter.makeHtml(input);
    };
  })
  .filter("unique", function () {
    return function (input) {
      return _.uniq(input);
    };
  })
  .filter("trust", function ($sce) {
    return function (input) {
      return $sce.trustAsHtml(input);
    };
  });
