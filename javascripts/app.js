angular
  .module("Relais-3", [
    "ngRoute",
    "ngCookies",
    "ngSanitize",
    "ngAnimate",
    "Services",
    "Filters"
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when("/", {
        templateUrl: "views/main.html",
        controller: "MainCtrl"
      })
      .when("/signin", {
        templateUrl: "views/signin.html",
        controller: "SigninCtrl"
      })
      .when("/register", {
        templateUrl: "views/register.html",
        controller: "RegisterCtrl"
      })
      .otherwise({
        redirectTo: "/signin"
      });
  });
